> ## `George Tsatsis | i write code from time to time.. i guess?`
> 
> <details><summary><h3>Libraries</h3></summary>
>     <ul>
>         <li>[anticaptcha (`george/anticaptcha`)](https://gitlab.com/george/anticaptcha): A Go library to interface with the [anti-captcha](https://anti-captcha.com) service.</li>
> </details>
> 
> <img align="center" src=https://selfhost.services/img/gif/55.gif></img>
